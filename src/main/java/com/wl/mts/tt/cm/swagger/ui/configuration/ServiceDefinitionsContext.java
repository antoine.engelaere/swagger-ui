package com.wl.mts.tt.cm.swagger.ui.configuration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import lombok.extern.slf4j.Slf4j;
import springfox.documentation.swagger.web.SwaggerResource;

/**
 * @author satish sharma
 *     <pre>
 *   	In-Memory store to hold API-Definition JSON
 * </pre>
 */
@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_SINGLETON)
@Slf4j
public class ServiceDefinitionsContext {

  public final File zip = Files.createTempDirectory("swagger-uis").toFile();
  private final ConcurrentHashMap<String, String> serviceDescriptions = new ConcurrentHashMap<>();

  public ServiceDefinitionsContext() throws URISyntaxException, IOException {
    Assert.isTrue(this.zip.exists(), "");
  }

  public void addServiceDefinition(String serviceName, String serviceDescription) {
    try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zip))) {
      ZipEntry e = new ZipEntry(serviceName + ".json");
      out.putNextEntry(e);

      byte[] data = serviceDescription.getBytes();
      out.write(data, 0, data.length);

      out.closeEntry();
    } catch (Exception e) {
      log.error("", e);
    }

    serviceDescriptions.put(serviceName, serviceDescription);
  }

  public String getSwaggerDefinition(String serviceId) {
    return this.serviceDescriptions.get(serviceId);
  }

  public List<SwaggerResource> getSwaggerDefinitions() {
    return serviceDescriptions
        .entrySet()
        .stream()
        .map(
            serviceDefinition -> {
              SwaggerResource resource = new SwaggerResource();
              resource.setLocation("/service/" + serviceDefinition.getKey());
              resource.setName(serviceDefinition.getKey());
              resource.setSwaggerVersion("2.0");
              return resource;
            })
        .collect(Collectors.toList());
  }

  public File getZip() {
    return zip;
  }
}
