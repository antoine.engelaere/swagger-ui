package com.wl.mts.tt.cm.swagger.ui.configuration;

import java.io.IOException;
import java.util.Arrays;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

@Component
@RequiredArgsConstructor
@Slf4j
public class ServiceDescriptionUpdater {

  //  private final DiscoveryClient discoveryClient;
  private final ServiceDefinitionsContext definitionContext;
  private final ObjectMapper objectMapper;
  private final RestTemplate template = new RestTemplate();

  //  @Scheduled(fixedDelayString = "${swagger.config.refreshrate}")
  public void refreshSwaggerConfigurations() {
    log.debug("Starting Service Definition Context refresh");

    //    discoveryClient
    //        .getServices()
    Arrays.asList("cm-lookup-functions", "cm-uid-manager")
        .forEach(
            serviceId -> {
              ResponseEntity<String> response =
                  template.getForEntity(
                      "http://" + serviceId + "-svc:8080/v2/api-docs?group=" + serviceId,
                      String.class);
              try {
                val json = (ObjectNode) objectMapper.readTree(response.getBody());
                json.replace(
                    "host",
                    TextNode.valueOf("cm-gateway-tpd-dev-pf-components-private.wldp.fr"));
                json.replace(
                        "scheme",
                        TextNode.valueOf("https"));
                definitionContext.addServiceDefinition(serviceId, json.toString());
              } catch (IOException e) {
                log.error("", e);
              }
            });
  }
}
