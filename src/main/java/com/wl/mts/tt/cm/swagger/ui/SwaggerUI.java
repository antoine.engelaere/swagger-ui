package com.wl.mts.tt.cm.swagger.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.wl.mts.tt.cm.swagger.ui.configuration.ServiceDescriptionUpdater;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
// @EnableDiscoveryClient
@SpringBootApplication
public class SwaggerUI {

  public static void main(String[] args) {
    final ConfigurableApplicationContext ctx = SpringApplication.run(SwaggerUI.class, args);
    ctx.getBean(ServiceDescriptionUpdater.class).refreshSwaggerConfigurations();
  }
}
