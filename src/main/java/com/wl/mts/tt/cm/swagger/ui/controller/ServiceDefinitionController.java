package com.wl.mts.tt.cm.swagger.ui.controller;

import java.net.MalformedURLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.wl.mts.tt.cm.swagger.ui.configuration.ServiceDefinitionsContext;

@RestController
public class ServiceDefinitionController {

  @Autowired private ServiceDefinitionsContext definitionContext;

  @GetMapping("/service/{servicename}")
  public String getServiceDefinition(@PathVariable("servicename") String serviceName) {
    return definitionContext.getSwaggerDefinition(serviceName);
  }

  @GetMapping("/swagger-ui-zip")
  public ResponseEntity<UrlResource> downloadAll() throws MalformedURLException {
    return ResponseEntity.ok()
        //            .contentType(MediaType.parseMediaType(contentType))
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=a.zip")
        .body(new UrlResource(definitionContext.zip.toURI()));
  }
}
